Welcome to apl42
------------------
APL is "A Programming Language"; its strength is in the
ease with which a programmer can manipulate arrays of 
numbers.

apl42 is a GPLed open source version of APL based on the
older freely available openAPL and apl\11.  The core apl42 
language level is roughly that of IBM APL\360.

apl\11 was originally written for Unix Version 6 running on
a DEC PDP-11 minicomputer.  apl42 is a fork of openAPL
available [here](https://github.com/PlanetAPL/openAPL).  A 
version of the original apl\11, which
will probably not compile under any modern C compiler, is
available [here](https://github.com/Lobachevsky/APL11) 
for reference.

apl42 uses standard ASCII with digraphs (such as `<=`, `>=`, `[]`, `:=`) 
and trigraphs (such as `[']` and `[%]`) as
the need was to have a version of APL running in a Linux
container and Termux, an Android Linux terminal, places where
lightweight input handling was needed and 
APL keyboard support would be problematic or impractical.  The
APL high negative symbol has been removed as the lexical analysis rule is
that the minus sign binds to the number to its right.  A
space will separate a minus sign from an adjacent number.  An
effort was made to keep frequently-used characters as single characters for 
brevity.  Usefully redundant symbols for operations over the first coordinate, 
such as compression and expansion, reduction and scan, rotate and reverse, and 
catenation and ravel have been eliminated in
an effort to reduce the number of digraphs and trigraphs.  Lastly, use two
adjacent dots for outer product, as in `1 2 3 ..+ 1 2 3`.

apl42 will not win any speed awards as only one datatype,
`double`, has been used for data.  Even character scalars and
vectors are represented 
internally as `double`.  Nor are there any new features such
as nested arrays, object orientation, or hooks to the system
such as `[]na`.  Language parsing is handled by `bison`, an
open-source successor to `yacc`, a state-of-the-art 
parser generator of the 
1970s.  Digraph and trigraph handling are done by a simple
input preprocessor - as the yacc parser implementation depends
on single APL characters, error messages will be missing
the digraphs and trigraphs as originally coded.

Specialised I-Beam functions may be added to return things like
phone location data in the future.

For high-quality open source APL systems, see `GNU APL`, an 
`IBM APL2` compatible system for Linux, 
available [here](https://www.gnu.org/software/apl/) and 
`NARS2000`, an experimental system for trying
out new language features which runs on Windows, 
available [here](https://www.nars2000.org/).

Testing the Water
-----------------

This version of apl42 is currently maintained under Ubuntu 20.04
and should work, that is, successfully compile, link, and 
execute, with no changes.  You should have the following 
installed and working on your Linux system:
- make (currently using version 4.2.1, should come with Linux)
- git (currently using version 2.25.1)
- gcc (currently using version 9.3.0, should come with Linux)
- bison (currently using version 3.5.1)

You should have some very basic knowledge of git.

Required Additional Packages
----------------------------
In addition to openAPL, you may need some of the following:
1. If you intend to run openAPL under X11, download a copy
   of the source code for rxvt4apl from the same location as
   openAPL.  Note, RedHat Linux users may be able to use
   the rxvt that comes with their distribution.
2. If you want to print APL functions on a Postscript(TM)
   compatible printer, get a copy of a2gs (by the way, a2ps 
   will not do).
3. If you want to print APL functions on an Epson(TM) or
   compatible printer, get a copy of fprint, (ie fontprint).

Instructions
------------
Instructions for compiling and installing openAPL are located at:

...openAPL/docs/install_guide/Installation	and
...openAPL/docs/install_guide/terminals

To summarise, these are the major steps:
1. Edit the file 'configure' to select options such as destination
   directory and print filters.  Run the commands:

	$> ./configure
	$> make
and	$> make install		#as root

2. Test your Linux virtual console and/or the rxvt program for
   8-bit clean operation, (if necessary) configure, make and
   install rxvt4apl.

3. Install a print filter (optional). 

4. Fine tune the control files for things such as your preferred
   editor and X11 font.


Documentation
------------
It is assumed that the user is familiar with APL or has
some other resource for learning it - if not, refer to
the file ...openAPL/docs/project/Resources.  User
instructions specific to openAPL can be found at 

...openAPL/docs/user_guide

Copyright
---------
openAPL, Copyright (C) Branko Bratkovic 1998, 1999
openAPL is free software and is covered by the GNU General
Public License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

For more details see the GNU General Public License (GPL) in
the directory  ...openAPL/docs.
