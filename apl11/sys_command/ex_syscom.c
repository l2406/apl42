
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include "apl.h"
#include "utility.h"
#include "opt_codes.h"
#include "data.h"
#include "work_space.h"
#include "main.h"
#include "debug.h"
#include "userfunc.h"
#include "parser.h"
#include "listdir.h"
#include "ex_shell.h"
#include "ex_list.h"
#include "ex_prws.h"
#include "quad_var.h"

void updatePrintP(struct item *p);

void ex_syscom() {
    int i, *ip, j;
    struct item* p;
    SymTabEntry* n;
    char fname[64]; /* Array for filename */
    char *cp, *vfname();

    i = *gsip->ptr++;
    switch (i) {

    default:
        error(ERR_implicit, "unrecognised system function");

    case SCRIPT:
        SECURITY_CHECK;
        if (protofile > 0)
            close(protofile);
        protofile = 0;
        cp = vfname(fname);
        if (equal(cp, "off"))
            return;
        if ((protofile = open(cp, 1)) > 0) {
            lseek(protofile, 0L, SEEK_END); /* append to existing file */
            printf("[appending]\n");
        }
        else {
            /*
          * create new file
          */
            protofile = opn(cp, 0644);
            printf("[new file]\n");
        }
        writeErrorOnFailure(protofile, "\t)script on\n", 12);
        return;

    case DEBUG:
        code_trace = !code_trace;
        stack_trace = !stack_trace;
        funtrace = !funtrace;
        return;

    case MEMORY:
        mem_dump();
        return;

    case DIGITS:
        if (exprOrNullFlag) {
            p = sp[-1];
            sp--;
            updatePrintP(p);
            outputPrintP();

        } else {
            outputPrintP();
        }
        return;

    case TRACE:
        funtrace = 1;
        return;

    case UNTRACE:
        funtrace = 0;
        return;

    case WRITE:
        funwrite(0);
        return;

    case DELL:
        sp[0] = sp[-1]; /*   duplicate top of stack  */
        sp++;
        funwrite(scr_file);
        funedit(scr_file);
        unlink(scr_file);
        return;

    case EDIT:
        SECURITY_CHECK;
        funedit(0);
        return;

    case READ:
        funread(0);
        return;

    case ERASE:
        p = sp[-1];
        sp--;
        // purge_name(p);
        erase((SymTabEntry*)p);
        if (vars_trace)
            vars_dump();
        return;

    case CONTIN:
        i = opn("continue", 0644);
        wssave(i);
        printf(" continue");

    case OFF:
        Exit(0);

    case VARS:
        symtabIterateInit();
        while (n = symtabIterate()) {
            if (n->itemp && n->entryUse == DA) {
                if (column + 8 >= pagewidth)
                    printf("\n\t");
                printf(n->namep);
                putchar('\t');
            }
        }
        putchar('\n');
        return;

    case FNS:
        symtabIterateInit();
        while (n = symtabIterate()) {
            if (n->entryUse == DF || n->entryUse == MF || n->entryUse == NF) {
                if (column + 8 >= pagewidth)
                    printf("\n\t");
                printf(n->namep);
                putchar('\t');
            }
        }
        putchar('\n');
        return;

    case CODE:
        n = (SymTabEntry*)sp[-1];
        sp--;
        switch (n->entryUse) {
        default:
            error(ERR_implicit, "function name not found");
        case NF:
        case MF:
        case DF:
            if (n->itemp == 0)
                funcomp(n);
            #if 0
            ip = (int*)n->itemp;
            printf(" [p] "); /* prologue */
            code_dump(ip[1], 0);
            for (i = 1; i < *ip; i++) {
                printf(" [%d] ", i);
                code_dump(ip[i + 1], 0);
            }
            printf(" [e] "); /* epilogue */
            code_dump(ip[i + 1], 0);
            printf("\n");
            #endif
        {
            fprintf(stderr, "[p] ");
            code_dump(n->functionLines[0]->pcode, 0);
            for (i = 1; i < n->functionLineCount; i++) {
                fprintf(stderr, "[%d] ", i);
                code_dump(n->functionLines[i]->pcode, 0);
            }
            fprintf(stderr, "[e] ");
            code_dump(n->functionLines[n->functionLineCount]->pcode, 0);
        }
        }
        return;

    case SICLEAR:
        while (gsip != &prime_context) {
            ex_ibr0();
        }
        longjmp(cold_restart, 0);

    case SICOM:
        tback(1);
        return;

    case CLEAR:
        clear();
        printf("clear ws\n");
        goto warp1; /* four lines down, or so... */

    case LOAD:
        j = opn(vfname(fname), O_RDONLY);
        clear();
        wsload(j);
        printf(" %s\n", fname);
        eval_qlx(); /* possible latent expr evaluation */
    warp1:
        /*
       * this garbage is necessary because clear()
       * does a brk(&end), and the normal return & cleanup
       * procedures are guaranteed to fail (miserably).
       *      --jjb 1/78
       */
        sp = stack;
        longjmp(cold_restart, 0);
    //longjmp(reset_env, 0);
    //longjmp(gsip->env, 0);

    case LIB:
        listdir();
        return;

    case COPY:
        if (gsip != &prime_context) {
            error(ERR_implicit, "si damage -- type ')sic'");
        }
        wsload(opn(vfname(fname), 0));
        printf(" copy %s\n", fname);
        return;

    case DROPC:
        SECURITY_CHECK;
        cp = vfname(fname);
        if (unlink(cp) == -1)
            printf("[can't remove %s]\n", cp);
        return;

    case SAVE:
        SECURITY_CHECK;
        j = opn(vfname(fname), 0644);
        wssave(j);
        printf(" saved %s\n", fname);
        return;

    case SHELL:
        SECURITY_CHECK;
        ex_shell();
        return;

    case LIST:
        ex_list();
        return;

    case PRWS:
        ex_prws();
        return;

    case LICENSE:
        printf("apl42, Copyright (C) Branko Bratkovic 1998, 1999\n");
        printf("This program is free software; you can redistribute it and/or modify\n");
        printf("it under the terms of the GNU General Public License as published by\n");
        printf("the Free Software Foundation; either version 2 of the License, or\n");
        printf("(at your option) any later version.\n\n");
        printf("This program is distributed in the hope that it will be useful,\n");
        printf("but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
        printf("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n");
        printf("GNU General Public License for more details.\n\n");
        printf("You should have received a copy of the GNU General Public License\n");
        printf("along with this program; if not, write to the Free Software\n");
        printf("Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n");
        return;

    case HELP:
        printf("apl42 Vade Mecum\n");
        printf("Monadic form f x       SCALAR FUNCTIONS        Dyadic form y f x\n");
        printf("----------------                               -----------------\n");
        printf("x                      Conjugate       |  + |  Plus     4 + 6.4  ==> 10.4\n");
        printf("0 - x                  Negative        |  - |  Minus    4 - 6.4  ==> -2.4\n");
        printf("(x > 0) - x < 0        Signum          |  * |  Times    4 * 6.4  ==> 25.6\n");
        printf("1 %% x                  Reciprocal      |  %% |  Divide   4 %% 6.4  ==> .625\n");
        printf("x |+ - x               Magnitude       |  | |  Residue  x - y * |- y % x + x = 0\n");
        printf("Integer part           Floor           | |- |  Minimum  (x * x < y) + y * x >= y\n");
        printf("- |- - x               Ceiling         | |+ |  Maximum  - (- y) |- -x\n");
        printf("2.71828 ** x           Exponential     | ** |  Power    3 ** 4 ==> 81\n");
        printf("** x inverse           Natural Log     | *% |  Log      Log x base y\n");
        printf("x * 3.14159            Pi times        |  @ |  Circular, hyperbolic, Pythagorean\n");
        printf("*/ # x or                              |    |    (below left)\n");
        printf("  gamma(x+1)           Factorial       |  ! |  Binomial (!x) % (!y) * !x - y\n");
        printf("1 - x [x=0 or 1]       Not             |  ~ |  Not defined\n");
        printf("-------------------------------------  |    |  -----------------------------------\n");
        printf("       (-y)@x      y       y@x         |    |                y   x  &&  ||  ~&  ~|\n");
        printf("-------------------------------------  |    |  -----------------------------------\n");
        printf("((-1)-x**2)**0.5 | 0 | (1-x**2)**0.5   | && |  And           0   0   0   0   1   1\n");
        printf("Arcsin x         | 1 | Sin x           | || |  Or            0   1   0   1   1   0\n");
        printf("Arccos x         | 2 | Cos x           | ~& |  Not and       1   0   0   1   1   0\n");
        printf("Arctan x         | 3 | Tan x           | ~| |  Not or        1   1   1   1   0   0\n");
        printf("((-1)+x**2)**0.5 | 4 | (1+x**2)**0.5   |  < |  Less              Relations:\n");
        printf("Arcsinh x        | 5 | Sinx x          | <= |  Not greater       Result is 1\n");
        printf("Arccosh x        | 6 | Cosh x          |  = |  Equal             if relation\n");
        printf("Arctahn x        | 7 | Tanh x          | >= |  Not less          holds and 0\n");
        printf("                                       |  > |  Greater           if not; e.g.\n");
        printf("                                       | ~= |  Not equal         5 <= 8 ==> 1\n");

        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");

        return;

    case VOGON:
        printf("Oh freddled gruntbuggly,\n");
        printf("Thy micturations are to me\n");
        printf("As plurdled gabbleblotchits\n");
        printf("On a lurgid bee.\n");
        printf("That mordiously hath bitled out its earted jurtles\n");
        printf("Into a rancid festering.\n");
        printf("Now the jurpling slayjid agrocrustles\n");
        printf("Are slurping hagrily up the axlegrurts,\n");
        printf("And living glupules frart and slipulate\n");
        printf("Like jowling meated liverslime.\n");
        printf("Groop, I implore thee, my foonting turlingdromes,\n");
        printf("And hooptiously drangle me\n");
        printf("With crinkly bindlewurdles,\n");
        printf("Or else I shall rend thee in the gobberwarts with my blurglecruncheon.\n");
        printf("See if I don't.\n");
        return;
    }
}

char* vfname(char* array) {
    SymTabEntry* n;
    char* p;

    n = (SymTabEntry*)sp[-1];
    sp--;
    if (n->entryType != LV) { error(ERR_value, "not a local varaible"); }
    p = n->namep;
    while (*array++ = *p++)
        ;
    return (n->namep);
}
