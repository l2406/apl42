/* ascii_input.c, Copyright (C) 2016, Greg Johnson
 * Released under the terms of the GNU GPL v2.0.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "apl.h"
#include "ascii_input.h"
#include "char.h"
#include "memory.h"
#include "utility.h"

static void init();
static char* rline(char* source);
static char apl_xlate[256];
static bool inited = false;

void dbprint(const char* title, char* p) {
    int i;
    fprintf(stderr, "%s >>", title);
    for (i = 0; i < strlen((char*)p); ++i) {
        if (p[i] >= 32 && p[i] <= 126)
            fprintf(stderr, "%c", p[i]);
        else
            fprintf(stderr, "<%d>", (int)((unsigned char*)p)[i]);
    }
    fprintf(stderr, "<<\n");
}

char* to_ascii_input(char* input) {
    if (!inited) { init(); }
    char* result;
    int i;
    result = rline(input);

    // int len = strlen(result);
    // for (i = 0; i < len; ++i) {
    //     result[i] = apl_xlate[(unsigned char)result[i]];
    // }

    return result;
}

static struct {
    char in;
    char out;
} charmap[] = {
    { '$', C_RHO },
    { '#', C_IOTA },
    // { '@', C_JOT },
    // { '|', C_OR },
    { '*', C_MULTIPLY },
    { '%', C_DIVIDE },
    { '|', C_STILE },
    // { 'D', C_DOWNSTILE },
    // { 'S', C_UPSTILE },
    { '@', C_CIRCLE },
    // { 'N', C_DOWNTACK },
    // { '^', C_UPARROW },
    // { 'U', C_DOWNARROW },
    // { 'E', C_EPSILON },
    // { 'B', C_UPTACK },
    // { 'G', C_DEL },
    // { 'H', C_DELTA },
};

static void init() {
    int i;
    for (i = 0; i < 256; ++i) { apl_xlate[i] = i; }               // initial translate table
    for (i = 0; i < sizeof(charmap) / sizeof(charmap[0]); ++i) {  // populate translate table
        apl_xlate[charmap[i].in] = charmap[i].out;
    }
    inited = true;
}

static struct {
    char in[2];
    char out;
} digraphs[] = {
    { "+!", C_UPTACK },
    { "-!", C_DOWNTACK },
    { "<=", C_LESSOREQUAL },
    { ">=", C_GRATOREQUAL },
    { "[]", C_QUAD },
    { ":=", C_LEFTARROW },
    { "~>", C_RIGHTARROW },
    { "**", '*' }, // exponentiation
    { "?=", C_EPSILON },
    { "^~", C_UPARROW },
    { "~^", C_DOWNARROW },
    // { "()", C_CIRCLE },
    // { "-/", C_SLASHBAR },     /* 0200 compress */
    // { "-\\", C_SLOPEBAR },    /* 0201 expand */
    { "&&", '^' }, // and
    { "||", C_OR },
    { "~&", C_NAND },         /* 0205 nand */
    { "~|", C_NOR },          /* 0206 nor */
    { "*%", C_CIRCLESTAR },   /* 0207 log */
    // { "-O", C_CIRCLESTILE },  /* 0211 rotate */
    // { "O\\", C_CIRCLESLOPE }, /* 0212 transpose */
    // { "BN", C_IBEAM },        /* 0213 i beam */
    // { "%L", C_QUADDIVIDE },
    // { "A|", C_DELTASTILE },   /* 0215 grade up */
    // { "V|", C_DELSTILE },     /* 0216 grade dn */
    // { "O|", C_CIRCLESTILE },  /* 0217 rotate */
    // { "<=", C_LESSOREQUAL },  /* 0220 less eq */
    // { "=>", C_GRATOREQUAL },  /* 0221 greater eq */
    { "~=", C_NOTEQUAL },     /* 0222 not eq */
    // { "^~", C_NAND },         /* 0223 nand */
    // { "H|", C_DELTASTILE },   /* 0227 another grade up */
    // { "G|", C_DELSTILE },     /* 0230 another grade down */
    { "$$", C_UPTACKJOT },    /* 0241 standard execute */
    { "##", C_DOWNTACKJOT },  /* 0242 format */
    { "|+", C_UPSTILE } ,
    { "|-", C_DOWNSTILE },
    { "//", C_UPSHOEJOT },    /* 0246 lamp (comment delimiter) */
    { "<>", C_DIAMOND },      // diamond
    { "\0", '\0' }            /* two-characters of null ends list */
};

static struct {
    char in[3];
    char out;
} trigraphs[] = {
    { "[']", C_QUOTEQUAD },   /* 0202 quote quad */
    { "[|]", C_CIRCLESTILE },
    { "[\\]", C_CIRCLESLOPE },
    /// { "(*)", C_CIRCLESTAR },
    { "[%]", C_QUADDIVIDE },  // 0214 domino
    { "[^]", C_DELTASTILE }, // 0215 grade up
    { "[~]", C_DELSTILE },    // 0216 grade down
    // { "/-/", C_SLASHBAR },
    // { "\\-\\", C_SLOPEBAR },
    { "]-[", C_IBEAM },
    { "\0\0\0", '\0' } // end of list
};

static void addChar(char** result, int* len, int* nextIndex, char ch) {
    if (*result == NULL) {
        *result = (char*)alloc(32);
        *len = 32;
        *nextIndex = 0;
    }
    else if (*nextIndex == *len) {
        char* current = *result;
        *result = (char*)alloc(*len * 2);
        memcpy(*result, current, *len);
        aplfree((int*)current);
        *len *= 2;
    }

    (*result)[(*nextIndex)++] = ch;
}

static int digraphCmp(const char* a, const char* b) {
    int c = (a[0] - b[0]);
    if (c != 0) { return  c; } // exit early
    return a[1] - b[1];
}

static int trigraphCmp(const char* a, const char* b) {
    int c = (a[0] - b[0]);
    if (c != 0) { return c; } // exit early
    c = a[1] - b[1];
    if (c != 0) { return c; } // exit early
    return a[2] - b[2];
}

static char getDigraph(char first, char second) {
    int i;
    char pair[2];
    pair[0] = first;
    pair[1] = second;

    for (i = 0; digraphCmp(digraphs[i].in, "\0\0") != 0; ++i) {
        if (digraphCmp(digraphs[i].in, pair) == 0) { return digraphs[i].out; }
    }
    return 0;
}

static char getTrigraph(char first, char second, char third) {
    int i;
    char triplet[3];
    triplet[0] = first;
    triplet[1] = second;
    triplet[2] = third;

    for (i = 0; trigraphCmp(trigraphs[i].in, "\0\0\0") != 0; ++i) {
        if (trigraphCmp(trigraphs[i].in, triplet) == 0) { return trigraphs[i].out; }
    }
    return 0;
}


static int isNum(char arg) {
    return (arg == '.') || ((arg >= '0') && (arg <= '9'));
}

// manage character substitution for digraphs etc
static char* rline(char* source) {
    char* result = 0;
    int len = 32;
    int next = 0;
    int i;
    char c;

    // substitute ` for - when it appears directly in front of a number
    for (i = 0; source[i] != '\0'; ++i) {
        if (source[i] == '-' && isNum(source[i + 1])) { source[i] = '`'; }
    }

    // substitute jot for first dot of .. eg ..+
    for (i = 0; source[i] != '\0'; ++i) {
        if (source[i] == '.' && source[i + 1] == '.') { source[i] = C_JOT; }
    }

    // do the thing with nested quotes and comments some day

    for (i = 0; source[i] != '\0'; ++i) {
        c = getTrigraph(source[i], source[i + 1], source[i + 2]);
        if (c != 0) { i += 2; }   // move two extra character
        else { c = getDigraph(source[i], source[i + 1]);
            if (c != 0) { i += 1; } // move only one
            else { c = apl_xlate[(unsigned char)source[i]]; }
        }
        addChar(&result, &len, &next, c);
    }

    addChar(&result, &len, &next, '\n');
    addChar(&result, &len, &next, '\0');

    // dbprint("source was", source);
    // dbprint("rline returns", result);

    return result;
}

// old version of rline
static char* old_rline(char* source) {
    char* result = 0;
    int len = 32;
    int next = 0;
    int i;

    for (i = 0; source[i] != '\0'; ++i) {
        if (source[i + 1] != '@') {
            addChar(&result, &len, &next, source[i]);
        }
        else {
            char c = getDigraph(source[i], source[i + 2]);
            if (c == '\0') {
                error(ERR_syntax, "");
                break;
            }
            addChar(&result, &len, &next, c);
            i += 2;
        }
    }

    addChar(&result, &len, &next, '\n');
    addChar(&result, &len, &next, '\0');

    // dbprint("rline returns", result);

    return result;
}
